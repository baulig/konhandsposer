# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

import bpy
from bpy.types import (Operator, PropertyGroup, UIList, Panel, Scene)
from bpy.props import (EnumProperty, StringProperty, BoolProperty, PointerProperty, CollectionProperty)
from bpy.app.handlers import persistent
from bpy_extras.object_utils import world_to_camera_view
from xml.etree.ElementTree import ElementTree as Doc, Element as Root, SubElement as Tag
from mathutils import Vector


############ INFO ###############
bl_info = {
	"name": "Scene Annotation",
	"author": "Gerald Baulig",
	"version": (0, 1, 0),
	"blender": (2, 7 ,9),
	"location": "Properties > Render > Scene Annotation",
	"description": "Writes scene annotations to XML file",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Import-Export"}


################# Static Funcs ################
def relative(vec, mat):
	return (vec - mat.to_translation()) * mat


def embellish(root, linebreak, indent):
	embellish.depth = 0
	def recursive(elem):
		if len(elem):
			embellish.depth += 1
			elem.text = "{}{}{}".format(elem.text if elem.text else '', linebreak, embellish.depth*indent)
			for e in elem:
				recursive(e)
			embellish.depth -= 1
			e.tail = "{}{}{}".format(e.tail.strip(), linebreak, embellish.depth*indent)
		if embellish.depth:
			elem.tail = "{}{}{}".format(elem.tail if elem.tail else '', linebreak, embellish.depth*indent)
	recursive(root)
	pass


def annotate(scene=None):
	def meta(tag):
		meta_data = scene.annotation.meta
		tag = Tag(tag, 'meta')
		for m in meta_data:
			Tag(tag, m.tag, name=m.name).text = m.text
	
	def matrix(tag, mat, name=""):
		if scene:
			mat = scene.camera.convert_space(matrix=mat, from_space='WORLD', to_space='LOCAL')
		dim = "{}x{}".format(len(mat[0]), len(mat))
		inline = [[a for a in vec] for vec in mat].__str__()
		Tag(tag, 'matrix', name=name, dim=dim).text = inline
		pass
	
	def vertex(tag, vertex, name=""):
		if scene:
			pos = relative(vertex, scene.camera.matrix_world)
			uvd = world_to_camera_view(scene, scene.camera, vertex)
			c = uvd.x * scene.render.resolution_x
			r = uvd.y * scene.render.resolution_y
			return Tag(tag, 'vertex', name=name, x=str(pos.x), y=str(pos.y), z=str(pos.z), u=str(uvd.x), v=str(uvd.y), d=str(uvd.z), c=str(c), r=str(r))
		else:
			return Tag(tag, 'vector', name=name, x=str(vec.x), y=str(vec.y), z=str(vec.z))
	
	def bounds(tag, bounds, name=""):
		tag = Tag(tag, 'bounds', name=name)
		for v in bounds:
			vertex(tag, Vector(v))
		return tag
	
	def bone(tag, bone, verbose=False):
		tag = Tag(tag, 'bone', name=bone.name, parent=(bone.parent.name if bone.parent else ''))
		matrix(tag, bone.matrix)
		t = vertex(tag, bone.head, '{}.head'.format(bone.name))
		vertex(t, bone.tail, '{}.tail'.format(bone.name))
		if verbose:
			for child in bone.children:
				vertex(t, child.head, '{}.joint'.format(child.name))
		return tag
			
	def pose(tag, obj, hierarically=True):
		def recursive(tag, b):
			t = bone(tag, b)
			for c in b.children:
				recursive(t, c)
			pass
		
		if obj.pose: 
			tag = Tag(tag, 'pose', solver=obj.name)
			if hierarically:
				b = obj.pose.bones[0]
				recursive(tag, b)    
			else:
				for b in obj.pose.bones:
					bone(tag, b)
		else:
			print("Following object has no pose: {}".format(obj.name))
		return tag
	
		
	def object(tag, obj, hierarically=True):
		tag = Tag(tag, obj.type, name=obj.name, parent=(obj.parent.name if obj.parent else ''))
		matrix(tag, obj.matrix_world)
		if obj.type == 'MESH':
			bounds(tag, obj.bound_box)
		if obj.pose:
			pose(tag, obj)
		if hierarically:
			for child in obj.children:
				object(tag, child)
		return tag
	
	def objects(tag, objs, hierarically=True):
		for obj in objs:
			if obj.hide_render:
				continue
			if hierarically and obj.parent:
				continue
			object(tag, obj, hierarically)
		return tag
	
	annotate.meta = meta
	annotate.matrix = matrix
	annotate.vertex = vertex
	annotate.bounds = bounds
	annotate.bone = bone
	annotate.pose = pose
	annotate.object = object
	annotate.objects = objects
	return annotate 


def annotate_all(tag):
	annotate(bpy.data.scenes[0]).meta(tag)
	for scene in bpy.data.scenes:
		t = Tag(tag, 'scene', name=scene.name, cam=scene.camera.name)
		annotate(scene).objects(t, scene.objects)
	return tag


def format_filename(filename, ext='.xml'):
	from re import sub
	def repl_sharps(match):
		return str(bpy.context.scene.frame_current).zfill(len(match.group(0)))
	filename = sub('#+', repl_sharps, filename)
	filename = bpy.path.ensure_ext(filename, ext) 
	return bpy.path.abspath(filename)


def write_doc(root, outpath, linebreak='\n', indent='\t'):
	from os.path import dirname, isdir
	embellish(root, linebreak, indent)
	doc = Doc(root)
	outpath = bpy.path.abspath(format_filename(outpath, ".xml"))
	if isdir(dirname(outpath)):
		doc.write(outpath, encoding="UTF-8", xml_declaration=True)
		return {'INFO'}, "Annotations written to: {}".format(outpath)
	else:
		return {'ERROR'},  "Directory {} doesn't exist!".format(dirname(outpath))
		


############## Props ###############
class MetaAnnotationItem(PropertyGroup):
	bl_idname = "scene.annotation.meta"
	
	tag = StringProperty(
		name="Tag name",
		description="Name of the xml tag",
		maxlen=32,
		default="div")

	text = StringProperty(
		name="Text",
		description="Text for the tag body",
		default="")


class SceneAnnotationSetting(PropertyGroup):
	bl_idname = "scene.annotation.setting"
	
	method = EnumProperty(
		items= {
			('ACTIVE', 'Active', 'Annotate active selected Object'),
			('SELECTION', 'Selection', 'Annotate selected Object'),
			('HIERARCHY', 'Hierarchy', 'Annotate hierachically from active Object'),
			('ALL', 'All', 'All visible Objects')
			},
		name="Annotate",
		description="Annotate objects as xml",
		default='ACTIVE')
		
	when = EnumProperty(
		items= {
			('NEVER', 'Never', 'Annotation disabled'),
			('PRE', 'Pre Render', 'Annotation executs on pre render'),
			('POST', 'Post Render', 'Annotation executs on post render')
			},
		name="When",
		description="When annotations executes",
		default='NEVER')
		
	output = StringProperty(
		name="Output",
		description="Choose output path for annotation", 
		default="//Dataset\\",
		subtype='FILE_PATH')

	embellish = BoolProperty(
		name="Embellish",
		description="Prints XML with linebreaks and indented", 
		default=True)
	
	linebreak = EnumProperty(
		items= {
			('\n', 'LF', 'Linux style'),
			('\r\n', 'CRLF', 'Windows style'),
			('\r', 'CR', 'Mac style')
			},
		name="Linebreak",
		description="Choose linebreak type",
		default='\r\n')

	meta = CollectionProperty(
		type=MetaAnnotationItem,
		name='Metainfo',
		description="Space for aditional meta infos")


################ Operators #####################
class SceneAnnotation(Operator):
	"""Tooltip"""
	bl_idname = "scene.annotation"
	bl_label = "Annotate"
	bl_description = "Writes annotations to xml file of rendered scene"
	bl_space_type = "PROPERTIES"
	
	@classmethod
	def poll(self, context):
		return True

	def execute(self, context):
		root = Root('annotation')
		annotate_all(root)
		write_doc(root, context.scene.annotation.output)
		return {'FINISHED'}


############## UI ###############
class MetaAnnotationList(UIList):
	def draw_item(self, context, layout, item, icon, active_data, active_propname, index):
		if self.layout_type in {'DEFAULT', 'COMPACT'}:
			layout.label(item.tag)
		elif self.layout_type in {'GRID'}:
			layout.label(item.tag)
			

class SceneAnnotationPanel(Panel):
	"""Tooltip"""
	bl_idname = "RENDER_PT_Annotations"
	bl_label = "Scene Annotations"
	bl_space_type = "PROPERTIES"
	bl_region_type = "WINDOW"
	bl_category = "Render"
	bl_context = "render"
	
	def draw(self, context):
		layout = self.layout
		row = layout.row()
		row.prop(context.scene.annotation, 'method')
		row = layout.row()
		row.prop(context.scene.annotation, 'when')
		row = layout.row()
		row.prop(context.scene.annotation, 'output')
		row = layout.row()
		row.prop(context.scene.annotation, 'embellish')
		row.prop(context.scene.annotation, 'linebreak')
		row = layout.row()
		row.operator('scene.annotation')

		
############ Handler ############
@persistent
def on_pre_render(scene):
	if scene.annotation.when != 'PRE':
		return
	bpy.ops.scene.annotation()
	pass

@persistent
def on_post_render(scene):
	if scene.annotation.when != 'POST':
		return
	bpy.ops.scene.annotation()
	pass

	  
############ Register ############
def register():
	from bpy.utils import register_class
	from bpy.app import handlers
	register_class(MetaAnnotationItem)
	register_class(SceneAnnotationSetting)
	register_class(SceneAnnotation)
	register_class(SceneAnnotationPanel)
	Scene.annotation = PointerProperty(type=SceneAnnotationSetting)
	handlers.render_pre.append(on_pre_render)
	handlers.render_post.append(on_post_render)

def unregister():
	from bpy.utils import unregister_class
	from bpy.app import handlers
	del Scene.annotation
	unregister_class(MetaAnnotationItem)
	unregister_class(SceneAnnotationSetting)
	unregister_class(SceneAnnotation)
	unregister_class(SceneAnnotationPanel)
	handlers.render_pre.remove(on_pre_render)
	handlers.render_post.remove(on_post_render)
	


############## MAIN #############
if __name__ == "__main__":
	register()