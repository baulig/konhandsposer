# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

import random
import bpy
from bpy.types import (
	LimitLocationConstraint,
	LimitDistanceConstraint,
	LimitRotationConstraint,
	LimitScaleConstraint,
	PropertyGroup,
	Operator,
	Panel,
	Object,
	Scene
	)
from bpy.props import (
	PointerProperty,
	EnumProperty,
	StringProperty,
	BoolProperty
	)
from mathutils import Vector, Quaternion

############ INFO ###############
bl_info = {
	"name": "Pose Randomizer",
	"author": "Gerald Baulig",
	"version": (0, 1, 0),
	"blender": (2, 7 ,9),
	"location": "View3D > Pose Mode > Toolbar > Randomize",
	"description": "Randomize object pose along given limit constraints",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Animation"}
	
################# STATIC FUNCS #############
def randomize_location(obj, limits):
	changed = False
	if not isinstance(limits, LimitLocationConstraint):
		return changed
	if limits.use_min_x and limits.use_max_x:
		obj.location.x = random.uniform(limits.min_x, limits.max_x)
		changed = True
	if limits.use_min_y and limits.use_max_y:
		obj.location.y = random.uniform(limits.min_y, limits.max_y)
		changed = True
	if limits.use_min_z and limits.use_max_z:
		obj.location.z = random.uniform(limits.min_z, limits.max_z)
		changed = True
	return changed

	
def randomize_distance(obj, limits):
	if not isinstance(limits, LimitDistanceConstraint):
		return False
	rot = Quaternion((
		random.uniform(-1, 1),
		random.uniform(-1, 1), 
		random.uniform(-1, 1),
		random.uniform(-1, 1),
		))
	pos = Vector((1, 0, 0))
	pos.rotate(rot)
	pos = pos * limits.distance
	obj.location = pos
	return True


def randomize_rotation(obj, limits):
	changed = False
	if not isinstance(limits, LimitRotationConstraint):
		return changed
	if limits.use_limit_x:
		obj.rotation_euler.x = random.uniform(limits.min_x, limits.max_x)
		changed = True
	if limits.use_limit_y:
		obj.rotation_euler.y = random.uniform(limits.min_y, limits.max_y)
		changed = True
	if limits.use_limit_z:
		obj.rotation_euler.z = random.uniform(limits.min_z, limits.max_z)
		changed = True
	return changed


def randomize_scale(obj, limits):
	if not isinstance(limits, LimitScaleConstraint):
		return False
	min_s = max(limits.min_x, limits.min_y, limits.min_z)
	max_s = min(limits.max_x, limits.max_y, limits.max_z)
	scale = random.uniform(min_s, max_s)
	obj.scale.x = scale
	obj.scale.y = scale
	obj.scale.z = scale
	return True


def randomize_object(obj, verbose=False):
	if not hasattr(obj, 'constraints'):
		return {'WARNING'}, "%s has no constraints!" % obj.name if verbose else False
	
	changed = False
	for limits in obj.constraints:
		changed = randomize_location(obj, limits) or \
			randomize_distance(obj, limits) or \
			randomize_rotation(obj, limits) or \
			randomize_scale(obj, limits) or \
			changed
		pass
	
	if not verbose:
		return changed
	elif changed:
		return {'INFO'}, "%s randomized." % obj.name
	else:
		return {'WARNING'}, "%s has no limit constrants!" % obj.name


def randomize_selection(selection, verbose=False):
	count = 0
	for obj in selection:
		count += randomize_object(obj)
	if verbose:
		return {'INFO'}, "%d objects randomized" % count
	else:
		return count

		
def randomize_hierarchy(root, verbose=False):
	randomize_hierarchy.count = 0
	def recursive(parent):
		randomize_hierarchy.count += randomize_object(parent)
		for child in parent.children:
			recursive(child)
		pass
	
	recursive(root)
	if verbose:
		return {'INFO'}, "%d objects randomized" % randomize_hierarchy.count
	else:
		return randomize_hierarchy.count


def update_random_seed(self, context):
	random.seed(self.seed)
	pass


#################PROPS#######################
class PoseRandomizerSetting(PropertyGroup):
	bl_idname = "pose.randomizer.setting"

	armature = PointerProperty(
		name="Armature",
		description="Root bone for randomization",
		type=Object)


################# OPERATORS ################
class PoseRandomizer(Operator):
	"""Tooltip"""
	bl_idname = "pose.randomizer"
	bl_label = "Randomize"
	bl_description = "Randomize pose of armature"
	bl_space_type = "VIEW_3D"
	bl_options = {'REGISTER', 'UNDO'}
	
	method = EnumProperty(
		items= {
			('ACTIVE', 'Active', 'Randomize active selected PoseBone'),
			('SELECTION', 'Selection', 'Randomize selected PoseBones'),
			('HIERARCHY', 'Hierarchy', 'Randomize hierachically from active PoseBone'),
			('ALL', 'All', 'All visible PoseBone')
			},
		name="Method",
		description="Randomize bone pose.",
		default='ACTIVE')
	seed = StringProperty(
		name="Seed",
		description="Choose a random seed.", 
		default="",
		update=update_random_seed)
	keep = BoolProperty(
		name="Keep",
		description="Keep first state of random sequence.",
		default=False,
		update=update_random_seed)
	
	@classmethod
	def poll(cls, context):
		return True #context.area and \
			#context.area.type == 'VIEW_3D' and \
			#context.active_pose_bone is not None

	def execute(self, context):
		if self.keep:
			random.seed(self.seed)
			pass
		
		if context.area and context.area.type == 'VIEW_3D':
			if self.method == 'ALL':
				type, msg = randomize_selection(context.visible_pose_bones, True)
				self.report(type, msg)
			elif context.active_pose_bone is None:
				self.report({'WARNING'}, "No active selected pose_bone!")
			elif self.method == 'ACTIVE':
				type, msg = randomize_object(context.active_pose_bone, True)
				self.report(type, msg)
			elif self.method == 'SELECTION':
				type, msg = randomize_selection(context.selected_pose_bones, True)
				self.report(type, msg)
			elif self.method == 'HIERARCHY':
				type, msg = randomize_hierarchy(context.active_pose_bone, True)
				self.report(type, msg)
			else:
				self.report({'ERROR'}, "Unsupported method: %s" % self.method)
		else:
			if context.scene.randomizer.armature is None:
				self.report({'WARNING'}, "No active selected pose_bone!")
			elif self.method == 'ACTIVE':
				type, msg = randomize_object(context.scene.randomizer.armature.pose.bones[0], True)
				self.report(type, msg)
			elif self.method == 'HIERARCHY':
				type, msg = randomize_hierarchy(context.scene.randomizer.armature.pose.bones[0], True)
				self.report(type, msg)
			else:
				self.report({'ERROR'}, "Unsupported method: %s" % self.method)
		return {'FINISHED'}


################ Panel #####################
class PoseRandomizerPanel(Panel):
	"""Tooltip"""
	bl_idname = "POSEMODE_PT_Randomizer"
	bl_label = "Randomizer"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_category = "Tools"
	bl_context = "posemode"
	
	def draw(self, context):
		layout = self.layout
		row = layout.row()
		row.operator("pose.randomizer")
	

############ Register ############
def register():
	from bpy.utils import register_class
	register_class(PoseRandomizerSetting)
	register_class(PoseRandomizer)
	register_class(PoseRandomizerPanel)
	Scene.randomizer = PointerProperty(type=PoseRandomizerSetting)


def unregister():
	from bpy.utils import unregister_class
	unregister_class(PoseRandomizerSetting)
	unregister_class(PoseRandomizer)
	unregister_class(PoseRandomizerPanel)
	del Scene.randomizer


############## MAIN #############
if __name__ == "__main__":
	register()