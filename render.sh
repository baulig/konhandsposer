#! /usr/bin/env sh

SDIR=$(dirname "$SCRIPT")
START=${1:-1}
END=${2:-1000}
SEED=${3:-"Example"}

disp_help(){
	echo "Usage: $0 [START [END [SEED]]]"
	echo "    START: (INT)    Start frame of render process."
	echo "      END: (INT)    End frame of render process."
	echo "     SEED: (STRING) Seed for randomizer."
}

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
	disp_help
else
	blender "$SDIR/HandPoser.blend" -b -s $START -e $END -P "$SDIR/scripts/handposer.py" -a -- --seed="$SEED"
fi
