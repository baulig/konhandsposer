# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
import bpy

def lookup(wildcard):
    from glob import iglob
    return iglob(bpy.path.abspath(wildcard), recursive=True)

def plugin(name):
    if name in globals():
        print("INFO: {} already imported".format(name))
        return True
    import sys
    from os.path import dirname
    for match in lookup("//**/{}/__init__.py".format(name)):
        if not dirname(dirname(match)) in sys.path:
            sys.path.append(dirname(dirname(match)))
            print("INFO: append {}".format(match))
            return True
    print("WARNING: Could not find path to \"{}\"".format(name))
    return False
    
def addon(name, path=None):
    import addon_utils
    if not name in addon_utils.addons_fake_modules:
        print("WARNING: Addon \"{}\" not installed.".format(name))
        if path:
            for match in lookup(path):
                bpy.ops.wm.addon_install(overwrite=True, filepath=match)
                print("INFO: Addon \"{}\" installed.".format(name))
                break
        else:
            print("INFO: Addon \"{}\" could not be found.".format(name))
            return False
        
    _, state = addon_utils.check(name)
    if state:
        print("INFO: Addon \"{}\" is enabled.".format(name))
        return True
    try:
        addon_utils.enable(name, default_set=True, persistent=False)
        print("INFO: Addon \"{}\" enabled.".format(name))
        return True
    except:
        print("ERROR: Could not enable addon \"{}\".".format(name))
        return False

#####Test#######
if __name__ == "__main__":
    addon("annotation", "//**/annotation.py")
    addon("ranpose", "//**/ranpose.py")