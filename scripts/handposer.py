# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
#
# Author: Gerald Baulig

import random
import bpy

########## Environment ##########
SEED = 'HandPoser'
SOLVERLIB = {o.name:o for o in bpy.data.objects if o.pose}
SOLVER = SOLVERLIB['StraightSolver']
POSELIB = {a.name:a for a in bpy.data.actions}
POSES = POSELIB.keys()
BGD = bpy.data.images['Background']
BGD_PATHS = []
FRAME_PROTECT = None
FRAME_SEED = SEED

HEAD_PIVOT = bpy.data.objects['HeadPivot']
SUN = bpy.data.objects['Sun']

############ FUNCS ###############
def argparser(parents=None, prog=__file__):
	from argparse import ArgumentParser
	parser = ArgumentParser(
		parents=parents if parents else [],
		prog=prog
	)
	_, all = parser.parse_known_args()
	parser.add_argument('--solver', help="choose solver {}".format(SOLVERLIB.keys()), default=SOLVER.name)
	parser.add_argument('--poses', nargs='*', help="choose poses to be forced {}".format(POSELIB.keys()), default=POSELIB.keys())
	parser.add_argument('--seed', help="the random generation seed", type=str, default=SEED)
	parser.add_argument('--bgd', help="wildcard for searching background images", type=str, default='//Backgrounds/*.jpg')
	if '--' in all:
		args, _ = parser.parse_known_args(all[all.index('--')+1:])
	else:
		args, _ = parser.parse_known_args()
	return parser, args


def insert_meta(meta, name, text, tag=None):
	m = meta.get(name, None)
	if not m:
		m = meta.add()
	m.name = name
	m.tag = tag if tag else name
	m.text = text
	return m


def update_background():
	if BGD_PATHS:
		BGD.filepath = random.choice(BGD_PATHS)
		BGD.reload()
		

def update_frame_seed(scene):
	global FRAME_SEED
	FRAME_SEED = SEED + str(scene.frame_current)
	random.seed('{}'.format(FRAME_SEED))


def update_meta(scene):
	meta = scene.annotation.meta
	insert_meta(meta, 'frame', str(scene.frame_current))
	insert_meta(meta, 'seed', FRAME_SEED)
	pose_action = scene.randomizer.armature.animation_data.action
	insert_meta(meta, 'pose', pose_action.name if pose_action else 'None')
	insert_meta(meta, 'head_rgb', '{:>06}_head_rgb.png'.format(scene.frame_current), 'file')
	insert_meta(meta, 'head_dpt', '{:>06}_head_dpt.png'.format(scene.frame_current), 'file')
	insert_meta(meta, 'head_seg', '{:>06}_head_seg.png'.format(scene.frame_current), 'file')
	insert_meta(meta, 'right_rgb', '{:>06}_right_rgb.png'.format(scene.frame_current), 'file')
	insert_meta(meta, 'right_dpt', '{:>06}_right_dpt.png'.format(scene.frame_current), 'file')
	insert_meta(meta, 'right_seg', '{:>06}_right_seg.png'.format(scene.frame_current), 'file')


def update_pose(scene):
	print("INFO: Prepare pose {}".format(FRAME_SEED))
	bpy.ops.pose.randomizer(method='HIERARCHY')
	scene.randomizer.armature.animation_data.action = random.choice(POSES)


def update_dirty():
	#TODO fix this hack!!!
	HEAD_PIVOT.location.y = random.randint(-45, -10)
	SUN.rotation_euler[0] = random.random() * 3 - 1.5
	SUN.rotation_euler[1] = random.random() * 3 - 1.5
	SUN.rotation_euler[2] = random.random() * 3 - 1.5


def update_frame(scene):
	update_frame_seed(scene)
	update_pose(scene)
	update_background()
	update_meta(scene)
	update_dirty()


def on_frame_change(scene):
	global FRAME_PROTECT
	if FRAME_PROTECT != scene.frame_current:
		try:
			update_frame(scene)
		except:
			unregister()
			raise RuntimeError('Error in "on_frame_change". Module unregistered!')
		FRAME_PROTECT = scene.frame_current
	pass


def register():
	bpy.app.handlers.frame_change_pre.append(on_frame_change)
	pass

	
def unregister():
	bpy.app.handlers.frame_change_pre.remove(on_frame_change)
	pass


############# MAIN #################
if __name__ == '__main__':
	import sys
	from glob import glob
	###Inhire Scripts###
	scripts = bpy.path.abspath("//scripts")
	if not scripts in sys.path:
		sys.path.append(scripts)
	
	###Inhire Plugins & Addons###
	import inhire
	inhire.addon("annotation", "//**/annotation.py")
	inhire.addon("ranpose", "//**/ranpose.py")
	
	###Init args###
	_, args = argparser()
	SOLVER = SOLVERLIB[args.solver]
	POSES = [POSELIB[p] for p in args.poses]
	BGD_PATHS = glob(bpy.path.abspath(args.bgd))
	SEED = args.seed
	
	###Set start up defaults###
	bpy.context.scene.randomizer.armature = SOLVER
	bpy.context.scene.annotation.method = 'HIERARCHY'
	bpy.context.scene.annotation.when = 'POST'
	bpy.context.scene.annotation.meta.clear()
	register()